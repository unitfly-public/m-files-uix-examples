"use strict";
// Entry point of the shell ui application.
function OnNewShellUI(shellUI) {

    // Initialize the console.
    console.initialize(shellUI, "myAppName");

    // Wait for shell frames to be created.
    shellUI.Events.Register(MFiles.Event.NewNormalShellFrame, OnNewNormalShellFrame);
}

// Handler for shell frame created event.
function OnNewNormalShellFrame(shellFrame) {

    shellFrame.Events.Register(
        Event_Started,
        getShellFrameStartedHandler(shellFrame));
}

function getShellFrameStartedHandler(shellFrame) {
    /// <summary>Gets a function to handle the OnStarted event for an IShellFrame.</summary>

    // The shell frame is now started and can be used.
    return function () {
        // Register to be notified when a built-in command is executed.
        shellFrame.Commands.Events.Register(
            Event_BuiltinCommand,
            function (commandId, param) {
                /// <summary>Executed whenever a built-in command is clicked.</summary>
                /// <param name="commandId" type="BuiltinCommand">
                /// One of the built-in commands from the BuiltinCommand enumeration.
                /// ref: https://www.m-files.com/UI_Extensibility_Framework/#MFClientScript~BuiltinCommand.html
                /// </param> 
                /// <param name="param">
                /// If the <paramref name="commandId"/> is BuiltinCommand_NewObject then contains the object type id of the object to create (or -100 if not specified).
                /// Otherwise, returns -2.
                /// </param>
                /// <returns>A boolean defining whether the action should continue (true) or be cancelled (false).</returns>

                // Display every built-in command as message box.
                shellFrame.ShowMessage("Command ID: " + commandId + ", param: " + param);

                // UI ext app should return true when nothing is processed and want to continue default command behaviour.
                // Although default value is true when nothing is returned.
                // Return false to cancel the standard command execution.

                //DEMO 2
                return true;
            });
    };
}
