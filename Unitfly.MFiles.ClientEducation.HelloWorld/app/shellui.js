"use strict";
// Entry point of the shell ui application.
function OnNewShellUI(shellUI) {

    // Initialize the console.
    console.initialize(shellUI, "myAppName");

    // Wait for shell frames to be created.
    shellUI.Events.Register(MFiles.Event.NewNormalShellFrame, OnNewNormalShellFrame);
}

// Handler for shell frame created event.
function OnNewNormalShellFrame(shellFrame) {

    shellFrame.Events.Register(
        Event_Started,
        getShellFrameStartedHandler(shellFrame));
}

function getShellFrameStartedHandler(shellFrame) {

    return function () {
        shellFrame.ShowMessage("Hello World from a shell frame is available for use.");
    }
}
