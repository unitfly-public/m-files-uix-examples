"use strict";

function OnNewShellUI(shellUI) {

    shellUI.Events.Register(
        Event_NewShellFrame,
        handleNewShellFrame);
}

function handleNewShellFrame(shellFrame) {

    shellFrame.Events.Register(
        Event_Started,
        getShellFrameStartedHandler(shellFrame));
}

function getShellFrameStartedHandler(shellFrame) {

    return function () {

        // Create a command (button).  Note that it is not yet visible.
        // ref: https://www.m-files.com/UI_Extensibility_Framework/index.html#MFClientScript~ICommands~CreateCustomCommand.html
        var commandOneId = shellFrame.Commands.CreateCustomCommand("My First Command");

        // Add the command to the task area.
        // ref: https://www.m-files.com/UI_Extensibility_Framework/index.html#MFClientScript~ITaskPane~AddCustomCommandToGroup.html
        try {
            shellFrame.TaskPane.AddCustomCommandToGroup(commandOneId, TaskPaneGroup_Main, 1);
        }
        catch (e) {
            //Handle Error
        }

        //// DEMO#2
        // Add the command to the context menu.
        // ref: https://www.m-files.com/UI_Extensibility_Framework/index.html#MFClientScript~ICommands~AddCustomCommandToMenu.html
        shellFrame.Commands.AddCustomCommandToMenu(commandOneId, MenuLocation_ContextMenu_Top, 1);

        // Register to be notified when a custom command is clicked.
        // Note: this will fire for ALL custom commands, so we need to filter out others.
        shellFrame.Commands.Events.Register(
            Event_CustomCommand,
            function (commandId) {
                // Branch depending on the Id of the command that was clicked.
                switch (commandId) {
                    case commandOneId:
                        // Our first command was clicked.
                        shellFrame.ShowMessage("The first command was clicked.");

                        //// DEMO #3
                        shellFrame.Commands.SetCommandState(commandId,
                            CommandLocation_All,
                            CommandState_Inactive);
                        break;
                }
            });
    }
}